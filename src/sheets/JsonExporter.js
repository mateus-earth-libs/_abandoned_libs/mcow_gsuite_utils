//----------------------------------------------------------------------------//
//                       __      __                  __   __                  //
//               .-----.|  |_.--|  |.--------.---.-.|  |_|  |_                //
//               |__ --||   _|  _  ||        |  _  ||   _|   _|               //
//               |_____||____|_____||__|__|__|___._||____|____|               //
//                                                                            //
//  File      : JsonExporter.js                                               //
//  Project   : mcow_gsuite_utils                                             //
//  Date      : Nov 06, 2019                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2019                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//----------------------------------------------------------------------------//


//---------------------------------------------------------------------------//
// Public Functions                                                          //
//---------------------------------------------------------------------------//
//-----------------------------------------------------------------------------
function ExportAllSheets()
{
    const ss     = SpreadsheetApp.getActiveSpreadsheet();
    const sheets = ss.getSheets();

    var sheets_data = {};
    for (var i = 0, len = sheets.length; i < len; ++i) {
        var curr_sheet = sheets[i];

        const objs  = GetRows(curr_sheet);
        const name  = curr_sheet.getName();

        sheets_data[name] = objs;

    }
    const json_contents  = JSON.stringify(sheets_data);
    const folder_to_save = GetFormFolder();
    MakeFileToDownload(folder_to_save, json_contents);
}


//---------------------------------------------------------------------------//
// Helper Functions                                                          //
//---------------------------------------------------------------------------//
//-----------------------------------------------------------------------------
function GetRangeValues(sheet, row, col, maxRows, maxCols)
{
    const range = sheet.getRange(row, col, maxRows, maxCols);
    return range.getValues();
}

//-----------------------------------------------------------------------------
function TrimValues(arr)
{
  for(var i = arr.length -1; i >= 0; --i) {
    var value = arr[i];
    if(typeof(value) == "string" && String_IsEmptyOrNull(value)) {
       Array_PopBack(arr);
    } else if(typeof(value) == "object" && Object_IsEmptyOrNull(value)) {
       Array_PopBack(arr);
    }
  }
  return arr;
}

//-----------------------------------------------------------------------------
function GetRows(sheet) {
    // Headers.
    const sheet_frozen_rows = sheet.getFrozenRows();
    const sheet_max_rows    = sheet.getMaxRows   ();
    const sheet_max_cols    = sheet.getMaxColumns();

    const frozen_values = GetRangeValues(
        sheet,
        1, 1,
        sheet_frozen_rows,
        sheet_max_cols
    );
    const header_values = TrimValues(Array_GetLast(frozen_values));

    // Data.
    const data_values = GetRangeValues(
        sheet,
        sheet_frozen_rows + 1, 1,
        sheet_max_rows,
        header_values.length
    );

    const objs = GetObjects(data_values, header_values);
    return objs;
}

//-----------------------------------------------------------------------------
function GetObjects(data, keys)
{
    var objects = [];

    for (var i = 0; i < data.length; ++i) {
        var curr_row = TrimValues(data[i]);
        var curr_obj = {};
        // @notice(stdmatt): If found an empty row...
        // We're assuming that there's no data after that anymore.
        if(curr_row.length == 0){
           break;
        }

        for (var j = 0; j < curr_row.length; ++j) {
            var curr_value = curr_row[j];
            var curr_key   = keys    [j];

            curr_obj[curr_key] = curr_value;
        }
        objects.push(curr_obj);
    }

    return objects;
}


//-----------------------------------------------------------------------------
function GetFormFile()
{
    const curr_form = SpreadsheetApp.getActiveSpreadsheet();
    const form_file = DriveApp.getFileById(curr_form.getId());
    return form_file;
}

//-----------------------------------------------------------------------------
function GetFormFilename()
{
    const form_file     = GetFormFile();
    const form_filename = form_file.getName();
    return form_filename;
}

//-----------------------------------------------------------------------------
function GetFormFolder()
{
    const form_file = GetFormFile();
    const folder_it = form_file.getParents();
    if(folder_it.hasNext()) {
        var folder = folder_it.next();
        return folder;
    }
    return null;
}

//-----------------------------------------------------------------------------
function GetFormFoldername()
{
    const folder = GetFormFolder();
    if(!folder) {
        return "";
    }
    return folder;
}

//-----------------------------------------------------------------------------
function MakeFileToDownload(folder, contents)
{
    var file = folder.createFile('name.json', contents);
}


//---------------------------------------------------------------------------//
//                                                                           //
// MCOW Script Helpers                                                       //
//                                                                           //
//---------------------------------------------------------------------------//
//-----------------------------------------------------------------------------
function Array_GetLast(arr)
{
    if(arr && arr.length != 0) {
        return arr[arr.length -1];
    }
    return null;
}

//-----------------------------------------------------------------------------
function Array_PopBack(arr)
{
    arr.pop();
}

//-----------------------------------------------------------------------------
function String_IsEmptyOrNull(str)
{
    return !str || str == "";
}

//-----------------------------------------------------------------------------
function Object_IsEmptyOrNull(obj)
{
    return !obj || obj == {};
}
